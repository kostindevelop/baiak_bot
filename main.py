import telebot
import gspread
from telebot import types
from oauth2client.service_account import ServiceAccountCredentials
from datetime import datetime




# Укажите ваш токен Telegram-бота
TOKEN = '6423804869:AAFgfdCGyILT3JJk4RrOVTeH4-GvIGBl7h4'
bot = telebot.TeleBot(TOKEN)


# Укажите путь к файлу JSON с учетными данными для Google Sheets API
CREDENTIALS_FILE = '/Users/user/Documents/FirstBot/baiak_bot/omega-castle-398715-228939bffbab.json'

# Укажите имя вашей таблицы и листа
SPREADSHEET_NAME = 'WORK'

# Функция для подключения к Google Sheets
def connect_to_google_sheets():
    scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, scope)
    client = gspread.authorize(credentials)
    sh = client.open(SPREADSHEET_NAME)
    return sh

user_states = {}  # Создаем словарь для хранения состояний пользователей
failed_attempts = {}
user_failed_attempts = {}
user_data = {}


# Обработчик команды "/start"
@bot.message_handler(commands=['start'])
def start(message):
    # Устанавливаем состояние пользователя на 'main_menu'
    user_states[message.chat.id] = 'main_menu'

    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    item1 = telebot.types.KeyboardButton("Создать пост")
    item2 = telebot.types.KeyboardButton("Получить счет")
    item3 = telebot.types.KeyboardButton("Добавить клиента")
    item4 = telebot.types.KeyboardButton("Внести оплату")
    item5 = telebot.types.KeyboardButton("Кнопка 5")
    item6 = telebot.types.KeyboardButton("Удалить клиента")

    markup.add(item1, item2, item3, item4, item5, item6)

    bot.send_message(message.chat.id, "Выберите опцию:", reply_markup=markup)


# Обработчик команды "Создать пост"
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'main_menu' and message.text == "Создать пост")
def create_post(message):
    bot.send_message(message.chat.id, "Введите айди:")
    user_states[message.chat.id] = 'waiting_id'


# Обработчик текстовых сообщений в состоянии 'waiting_id'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_id')
def process_id(message):
    user_id = message.from_user.id
    user_first_name = message.from_user.first_name
    user_last_name = message.from_user.last_name if message.from_user.last_name else ''
    data = message.text

    # Если пользователь еще не имеет записи в словаре неудачных попыток, создаем ее
    if user_id not in failed_attempts:
        failed_attempts[user_id] = 0

    # Поиск айди в таблице ads
    try:
        # Подключаемся к Google Sheets
        scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
        credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, scope)
        gc = gspread.authorize(credentials)

        # Открываем таблицу и выбираем лист
        sh = gc.open(SPREADSHEET_NAME)
        worksheet = sh.worksheet("ads")

        # Получаем все записи из столбца A, B и C
        records = worksheet.get_all_values()

        # Создаем словарь, где ключ - айди, значение - (текст, изображение)
        id_to_data = {row[0]: (row[1], row[2]) for row in records}

        if data in id_to_data:
            # Если айди найдено в словаре, получаем текст и ссылку на изображение
            text, image_url = id_to_data[data]

            # Теперь у вас есть текст и URL изображения, который вы можете использовать для создания поста
            print(f"Текст: {text}")
            print(f"Изображение URL: {image_url}")

            # Отправляем сообщение с изображением и подписью
            bot.send_photo(message.chat.id, image_url, caption=text)
            channel_chat_id = '-1001870405673'
            # Создаем клавиатуру
            keyboard = types.InlineKeyboardMarkup()
            # Создаем кнопку "Оформить заказ" и привязываем ее к какой-либо команде или callback-кнопке
            order_button = types.InlineKeyboardButton("Оформить заказ", url=f"https://t.me/sale_bottiful_bot")
            keyboard.add(order_button)

            # Отправляем сообщение в канал с добавленной клавиатурой
            bot.send_photo(channel_chat_id, image_url, caption=text, reply_markup=keyboard)
            # bot.send_photo(channel_chat_id, image_url, caption=text)

            # Сбрасываем счетчик неудачных попыток
            failed_attempts[user_id] = 0

            # Пост создан успешно
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
            item1 = types.KeyboardButton("Да")
            item2 = types.KeyboardButton("Нет")
            markup.add(item1, item2)
            bot.send_message(message.chat.id, "Пост создан. Хотите создать еще один пост?", reply_markup=markup)
            user_states[message.chat.id] = 'waiting_create_another_post'
        else:
            print("Айди не найдено")
            bot.send_message(message.chat.id, "Айди не найдено. Пожалуйста, введите существующий айди.")

            # Увеличиваем счетчик неудачных попыток для данного пользователя
            failed_attempts[user_id] += 1

            # Если достигнуто 3 неудачных попытки, предлагаем перейти в главное меню
            if failed_attempts[user_id] >= 3:
                bot.send_message(message.chat.id, "Слишком много неудачных попыток. Вернуться в главное меню?",
                                 reply_markup=types.ReplyKeyboardRemove())
                start(message)  # Вызываем обработчик команды /start для возврата в главное меню

    except Exception as e:
        print(f"Ошибка при поиске айди: {str(e)}")
        bot.send_message(message.chat.id, "Произошла ошибка при поиске айди.")


# Обработчик текстовых сообщений в состоянии 'waiting_create_another_post'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_create_another_post')
def create_another_post(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите айди:")
        user_states[message.chat.id] = 'waiting_id'
    elif message.text.lower() == 'нет':
        # Возвращаем пользователя в главное меню
        start(message)
    else:
        bot.send_message(message.chat.id, "Пожалуйста, введите 'Да' или 'Нет'.")


# Здесь должна быть функция append_to_google_sheets

# Новое состояние 'waiting_name_lastname'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'main_menu' and message.text == "Получить счет")
def get_bill(message):
    bot.send_message(message.chat.id, "Введите имя и фамилию через пробел (например, Иван Иванов):")
    user_states[message.chat.id] = 'waiting_name_lastname'


# Обработчик текстовых сообщений в состоянии 'waiting_name_lastname'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_name_lastname')
def process_name_lastname(message):
    name_lastname = message.text.strip()

    # Подключаемся к Google Sheets
    scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE, scope)
    gc = gspread.authorize(credentials)

    try:
        # Открываем таблицу и выбираем лист bills
        sh = gc.open(SPREADSHEET_NAME)
        worksheet = sh.worksheet("bills")

        # Получаем все записи из столбца A, D, E, F
        records = worksheet.get_all_values()

        found = False  # Флаг для обозначения, была ли найдена строка с именем и фамилией

        for row in records:
            if name_lastname in row:
                # Если строка содержит введенное имя и фамилию
                found = True

                # Получаем значения из столбцов D, E, F
                d_value = row[1] if len(row) > 1 else ""
                e_value = row[2] if len(row) > 2 else ""
                f_value = row[3] if len(row) > 3 else ""

                response = f"Имя и фамилия: {name_lastname}\n"
                response += f"Доставка от поставщика: {d_value}\n"
                response += f"Было оплачено: {f_value}\n"
                response += f"Итого к оплате: {e_value}\n"

                bot.send_message(message.chat.id, response)

                # Сбрасываем счетчик неудачных попыток
                if message.chat.id in user_failed_attempts:
                    user_failed_attempts.pop(message.chat.id)

                # Предоставить другой счет
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
                item1 = types.KeyboardButton("Да")
                item2 = types.KeyboardButton("Нет")
                markup.add(item1, item2)
                bot.send_message(message.chat.id, "Предоставить другой счет?", reply_markup=markup)
                user_states[message.chat.id] = 'waiting_another_bill'

        if not found:
            # Увеличиваем счетчик неудачных попыток
            user_failed_attempts[message.chat.id] = user_failed_attempts.get(message.chat.id, 0) + 1

            if user_failed_attempts[message.chat.id] >= 3:
                # Если 3 неудачные попытки, предложить перейти в главное меню
                bot.send_message(message.chat.id, "Имя и фамилия не найдены в таблице bills. Пожалуйста, введите корректные данные.")
                markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
                item1 = telebot.types.KeyboardButton("Создать пост")
                item2 = telebot.types.KeyboardButton("Получить счет")
                item3 = telebot.types.KeyboardButton("Добавить клиента")
                item4 = telebot.types.KeyboardButton("Внести оплату")
                item5 = telebot.types.KeyboardButton("Кнопка 5")
                item6 = telebot.types.KeyboardButton("Удалить клиента")
                markup.add(item1, item2, item3, item4, item5, item6)
                bot.send_message(message.chat.id, "Вы можете вернуться в главное меню, выбрав опцию:", reply_markup=markup)
                user_states[message.chat.id] = 'main_menu'
            else:
                # В противном случае, предложить ввести данные снова
                bot.send_message(message.chat.id, "Имя и фамилия не найдены в таблице bills. Пожалуйста, введите корректные данные.")

    except Exception as e:
        print(f"Ошибка при поиске счета: {str(e)}")
        bot.send_message(message.chat.id, "Произошла ошибка при поиске счета.")




# Обработчик текстовых сообщений в состоянии 'waiting_another_bill'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_another_bill')
def waiting_another_bill(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите имя и фамилию через пробел (например, Иван Иванов):")
        user_states[message.chat.id] = 'waiting_name_lastname'
    elif message.text.lower() == 'нет':
        # Открываем кнопочное меню главного меню
        markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        item1 = telebot.types.KeyboardButton("Создать пост")
        item2 = telebot.types.KeyboardButton("Получить счет")
        item3 = telebot.types.KeyboardButton("Добавить клиента")
        item4 = telebot.types.KeyboardButton("Внести оплату")
        item5 = telebot.types.KeyboardButton("Кнопка 5")
        item6 = telebot.types.KeyboardButton("Удалить клиента")
        markup.add(item1, item2, item3, item4, item5, item6)
        bot.send_message(message.chat.id, "Выберите опцию из главного меню:", reply_markup=markup)
        user_states[message.chat.id] = 'main_menu'
    else:
        bot.send_message(message.chat.id, "Пожалуйста, введите 'Да' или 'Нет'.")

        # Увеличиваем счетчик неудачных попыток для данного пользователя
        user_failed_attempts[message.chat.id] = user_failed_attempts.get(message.chat.id, 0) + 1

        if user_failed_attempts[message.chat.id] >= 3:
            # Если 3 неудачные попытки, предложить перейти в главное меню
            bot.send_message(message.chat.id, "Имя и фамилия не найдены в таблице bills. Пожалуйста, введите корректные данные.")
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, "Выберите опцию из главного меню:", reply_markup=markup)
            user_states[message.chat.id] = 'main_menu'




# Обработчик команды "Добавить клиента"
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'main_menu' and message.text == "Добавить клиента")
def add_client(message):
    bot.send_message(message.chat.id, "Введите имя и фамилию через пробел (например, Иван Иванов):")
    user_states[message.chat.id] = 'waiting_add_client'

# Обработчик текстовых сообщений в состоянии 'waiting_add_client'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_add_client')
def process_add_client(message):
    user_id = message.from_user.id
    user_first_name = message.from_user.first_name
    user_last_name = message.from_user.last_name if message.from_user.last_name else ''
    full_name = message.text.strip()  # Получаем введенное имя и фамилию

    # Разделяем введенное имя и фамилию на отдельные части
    name_parts = full_name.split()

    if len(name_parts) < 2:
        bot.send_message(message.chat.id, "Пожалуйста, введите и имя, и фамилию.")
        return

    first_name = name_parts[0]
    last_name = ' '.join(name_parts[1:])

    # Проверяем, есть ли такое имя уже в таблице
    try:
        sh = connect_to_google_sheets()
        base_worksheet = sh.worksheet("base")

        names_column = base_worksheet.col_values(1)  # Получаем все значения из столбца A

        if full_name in names_column:
            bot.send_message(message.chat.id, "Этот клиент уже существует в базе.")
        else:
            # Если клиент с таким именем не существует, добавляем его
            name_lastname = f"{first_name} {last_name}"
            name_lastname = name_lastname.strip()  # Удаление лишних пробелов в начале и конце строки

            # Создаем список данных для добавления
            row_to_insert = [name_lastname]

            # Обернем этот список в другой список, так как Google Sheets ожидает двумерный массив
            values_to_insert = [row_to_insert]

            # Определяем, в какой строке добавить данные (последняя строка + 1)
            last_row = len(base_worksheet.get_all_values()) + 1

            # Вставляем данные в таблицу
            base_worksheet.insert_rows(values_to_insert, last_row)
            bot.send_message(message.chat.id, "Клиент успешно добавлен в базу данных.")
    except Exception as e:
        print(f"Ошибка при добавлении клиента: {str(e)}")
        bot.send_message(message.chat.id, "Произошла ошибка при добавлении клиента.")





    # Спрашиваем, нужно ли добавить еще клиентов
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    item1 = types.KeyboardButton("Да")
    item2 = types.KeyboardButton("Нет")
    markup.add(item1, item2)
    bot.send_message(message.chat.id, "Хотите добавить еще клиента?", reply_markup=markup)
    user_states[message.chat.id] = 'waiting_add_another_client'


# Обработчик текстовых сообщений в состоянии 'waiting_add_another_client'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_add_another_client')
def process_add_another_client(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите имя и фамилию следующего клиента:")
        user_states[message.chat.id] = 'waiting_add_client'
    elif message.text.lower() == 'нет':
        return_to_main_menu(message)
    else:
        bot.send_message(message.chat.id, "Пожалуйста, введите 'Да' или 'Нет'.")

# Функция для возвращения пользователя в главное меню
def return_to_main_menu(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    item1 = types.KeyboardButton("Создать пост")
    item2 = types.KeyboardButton("Получить счет")
    item3 = types.KeyboardButton("Добавить клиента")
    item4 = types.KeyboardButton("Внести оплату")
    item5 = types.KeyboardButton("Кнопка 5")
    item6 = types.KeyboardButton("Удалить клиента")
    markup.add(item1, item2, item3, item4, item5, item6)
    bot.send_message(message.chat.id, "Выберите опцию из главного меню:", reply_markup=markup)
    user_states[message.chat.id] = 'main_menu'


# Обработчик команды "Удалить клиента"
@bot.message_handler(
    func=lambda message: user_states.get(message.chat.id) == 'main_menu' and message.text == "Удалить клиента")
def delete_client(message):
    bot.send_message(message.chat.id, "Введите имя и фамилию клиента, которого хотите удалить:")
    user_states[message.chat.id] = 'waiting_delete_client_name_lastname'


# Обработчик текстовых сообщений в состоянии 'waiting_delete_client_name_lastname'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_delete_client_name_lastname')
def process_delete_client_name_lastname(message):
    name_lastname = message.text.strip()
    sh = connect_to_google_sheets()
    base_worksheet = sh.worksheet("base")  # Предположим, что лист называется "base"

    # Проверяем, есть ли имя в столбце A
    names_column = base_worksheet.col_values(1)  # Получаем все значения из столбца A
    if name_lastname in names_column:
        # Находим индекс строки с именем
        row_index = names_column.index(name_lastname) + 1

        # Удаляем клиента и значения его ячеек в столбцах B-E
        base_worksheet.update(f'A{row_index}', '')
        bot.send_message(message.chat.id, f"Клиент {name_lastname} успешно удален.")

        # После успешного удаления, спрашиваем, нужно ли удалить еще клиента
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        item1 = types.KeyboardButton("Да")
        item2 = types.KeyboardButton("Нет")
        markup.add(item1, item2)
        bot.send_message(message.chat.id, "Удалить еще клиента?", reply_markup=markup)
        user_states[message.chat.id] = 'waiting_delete_client_confirmation'
    else:
        bot.send_message(message.chat.id,
                         f"Клиент {name_lastname} не найден в базе. Пожалуйста, введите другое имя и фамилию.")


# Обработчик текстовых сообщений в состоянии 'waiting_delete_client_confirmation'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_delete_client_confirmation')
def process_delete_client_confirmation(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите имя и фамилию следующего клиента для удаления:")
        user_states[message.chat.id] = 'waiting_delete_client_name_lastname'
    elif message.text.lower() == 'нет':
        return_to_main_menu(message)
    else:
        bot.send_message(message.chat.id, "Пожалуйста, введите 'Да' или 'Нет'.")


# Обработчик команды "Внести оплату"
@bot.message_handler(
    func=lambda message: user_states.get(message.chat.id) == 'main_menu' and message.text == "Внести оплату")
def enter_payment(message):
    bot.send_message(message.chat.id, "Введите имя и фамилию клиента через пробел (например, Иван Иванов):")
    user_states[message.chat.id] = 'waiting_payment_name_lastname'


# Обработчик текстовых сообщений в состоянии 'waiting_payment_name_lastname'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_payment_name_lastname')
def process_payment_name_lastname(message):
    name_lastname = message.text.strip()
    sh = connect_to_google_sheets()
    base_worksheet = sh.worksheet("base")

    # Проверяем, есть ли имя в столбце A
    names_column = base_worksheet.col_values(1)  # Получаем все значения из столбца A
    if name_lastname in names_column:
        user_data[message.chat.id] = {'name_lastname': name_lastname}
        bot.send_message(message.chat.id, "Введите сумму оплаты:")
        user_states[message.chat.id] = 'waiting_payment_amount'
    else:
        # Создаем клавиатуру с кнопками "Да" и "Нет"
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        item1 = types.KeyboardButton("Да")
        item2 = types.KeyboardButton("Нет")
        markup.add(item1, item2)

        bot.send_message(message.chat.id, f"Клиент {name_lastname} не найден в базе. Хотите внести оплату еще раз?", reply_markup=markup)
        user_states[message.chat.id] = 'waiting_retry_payment'


# Обработчик текстовых сообщений в состоянии 'waiting_retry_payment'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_retry_payment')
def process_retry_payment(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите имя и фамилию клиента для внесения оплаты:")
        user_states[message.chat.id] = 'waiting_payment_name_lastname'
    elif message.text.lower() == 'нет':
        return_to_main_menu(message)
    else:
        bot.send_message(message.chat.id, "Пожалуйста, выберите 'Да' или 'Нет' для ответа.")

# Обработчик текстовых сообщений в состоянии 'waiting_payment_amount'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_payment_amount')
def process_payment_amount(message):
    payment_amount = message.text.strip()

    if not payment_amount.isdigit():
        bot.send_message(message.chat.id, "Пожалуйста, введите корректную сумму оплаты (число).")
        return

    payment_amount = int(payment_amount)  # Преобразуем в целое число

    # Получаем имя и фамилию из user_data
    user_data_for_chat = user_data.get(message.chat.id, {})
    name_lastname = user_data_for_chat.get('name_lastname')

    if name_lastname:
        sh = connect_to_google_sheets()
        base_worksheet = sh.worksheet("base")


        # Проверяем, есть ли имя в столбце A
        names_column = base_worksheet.col_values(1)  # Получаем все значения из столбца A
        if name_lastname in names_column:
            # Находим индекс строки с именем
            row_index = names_column.index(name_lastname) + 1

            # Обновляем значение суммы оплаты в колонке E
            cell_range = f'E{row_index}:E{row_index}'  # Указываем диапазон ячеек для обновления
            cell_list = base_worksheet.range(cell_range)  # Получаем список ячеек в указанном диапазоне

            # Заполняем каждую ячейку значением суммы оплаты
            for cell in cell_list:
                cell.value = payment_amount

            # Обновляем значения в ячейках
            base_worksheet.update_cells(cell_list)

            # Сообщаем, что сумма оплаты успешно записана
            bot.send_message(message.chat.id, f"Сумма оплаты {payment_amount} успешно записана для клиента {name_lastname}.")

            # Спрашиваем, нужно ли внести еще оплату
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
            item1 = types.KeyboardButton("Да")
            item2 = types.KeyboardButton("Нет")
            markup.add(item1, item2)
            bot.send_message(message.chat.id, "Внести еще оплату?", reply_markup=markup)
            user_states[message.chat.id] = 'waiting_payment_more'
        else:
            bot.send_message(message.chat.id, f"Клиент {name_lastname} не найден в базе. Пожалуйста, введите другое имя и фамилию.")
            user_states[message.chat.id] = 'waiting_payment_name_lastname'
    else:
        bot.send_message(message.chat.id, "Имя и фамилия клиента не найдены. Пожалуйста, начните с команды /add_payment.")

# Обработчик текстовых сообщений в состоянии 'waiting_payment_more'
@bot.message_handler(func=lambda message: user_states.get(message.chat.id) == 'waiting_payment_more')
def process_payment_more(message):
    if message.text.lower() == 'да':
        bot.send_message(message.chat.id, "Введите имя и фамилию клиента для внесения оплаты:")
        user_states[message.chat.id] = 'waiting_payment_name_lastname'
    elif message.text.lower() == 'нет':
        return_to_main_menu(message)
    else:
        bot.send_message(message.chat.id, "Пожалуйста, введите 'Да' или 'Нет' для ответа.")





if __name__ == '__main__':
    bot.remove_webhook()
    bot.polling(none_stop=True)